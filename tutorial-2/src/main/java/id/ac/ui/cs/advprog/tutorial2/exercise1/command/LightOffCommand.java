<<<<<<< tutorial-2/src/main/java/id/ac/ui/cs/advprog/tutorial2/exercise1/command/LightOffCommand.java
package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import id.ac.ui.cs.advprog.tutorial2.exercise1.receiver.Light;

public class LightOffCommand implements Command {

    private Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        // TODO Complete me!
        light.off();
    }

    @Override
    public void undo() {
        // TODO Complete me!
        light.on();
    }
}
=======
package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import id.ac.ui.cs.advprog.tutorial2.exercise1.receiver.Light;

public class LightOffCommand implements Command {

    private Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        // TODO Complete me!
        light.off();
    }

    @Override
    public void undo() {
        // TODO Complete me!
        light.on();
    }
}
>>>>>>> tutorial-2/src/main/java/id/ac/ui/cs/advprog/tutorial2/exercise1/command/LightOffCommand.java
