package tutorial.javari;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tutorial.javari.animal.Animal;

@RestController
public class JavariController {
    // TODO Implement me!
    JavariDatabase database = new JavariDatabase();

    public JavariController() throws IOException {
    }

    @GetMapping("/javari")
    public Object getAllAnimal() {
        List<Animal> listAnimal = database.getAnimals();
        if (listAnimal == null || listAnimal.isEmpty()) {
            return Message.nothingInDatabase();
        }
        return listAnimal;
    }

    @GetMapping("/javari/{idAnimal}")
    public Object getAnimalwithId(@PathVariable Integer idAnimal) {
        Animal animal = database.getAnimalwithId(idAnimal);
        if (animal == null) {
            return Message.notFoundMessage(idAnimal);
        }
        return animal;
    }

    @DeleteMapping("/javari/{idAnimal}")
    public Object deleteAnimal(@PathVariable Integer idAnimal) throws IOException {
        Animal animal = database.deleteAnimalwithId(idAnimal);
        if (animal != null) {
            Object[] messageAnimal = {Message.successDeleteAnimal(), animal};
            return messageAnimal;
        }
        return Message.notFoundMessage(idAnimal);
    }

    @PostMapping("/javari")
    public Object insertAnimal(@RequestBody String input) throws IOException {
        Animal animal = database.addNewAnimal(input);
        Object[] animalMessage = {Message.successAddAnimal(), animal};
        return animalMessage;
    }

}
