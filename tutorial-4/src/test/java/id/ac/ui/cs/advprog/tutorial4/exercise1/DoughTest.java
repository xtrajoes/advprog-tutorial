package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DoughTest {

    private Class<?> doughClass;
    private Class<?> thinCrustClass;
    private Class<?> thickCrustClass;
    private Class<?> noCrustClass;

    @Before
    public void setUp() throws Exception {
        doughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough");
        thinCrustClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough");
        thickCrustClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough");
        noCrustClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough");
    }

    @Test
    public void testDoughIsAPublicInterface() {
        int classModifiers = doughClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testDoughHasToStringMethod() throws Exception {
        Method toString = doughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());

    }

    @Test
    public void testThinCrustIsADoughBehavior() {
        Collection<Type> classInterfaces = Arrays.asList(thinCrustClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough")));
    }

    @Test
    public void testThinCrustOverrideToString() throws Exception {
        Method toString = thinCrustClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());

    }

    @Test
    public void testThickCrustIsADoughBehavior() {
        Collection<Type> classInterfaces = Arrays.asList(thickCrustClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough")));
    }

    @Test
    public void testThickCrustOverrideToString() throws Exception {
        Method toString = thickCrustClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());

    }


    @Test
    public void testNoCrustIsADoughBehavior() {
        Collection<Type> classInterfaces = Arrays.asList(noCrustClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough")));
    }

    @Test
    public void testNoCrustOverrideToString() throws Exception {
        Method toString = noCrustClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());

    }
}
