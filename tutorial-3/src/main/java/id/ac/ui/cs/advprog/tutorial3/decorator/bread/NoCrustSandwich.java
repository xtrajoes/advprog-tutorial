package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class NoCrustSandwich extends Food {
    public NoCrustSandwich() {
    }

    @Override
    public double cost() {
      //to do
        return 2.00;
    }

    @Override
    public String getDescription() {
      //to do
        return "No Crust Sandwich";
    }
}
