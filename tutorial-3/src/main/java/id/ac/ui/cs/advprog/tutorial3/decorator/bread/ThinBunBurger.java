package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThinBunBurger extends Food {
    public ThinBunBurger() {
    }

    @Override
    public double cost() {
      //to do
        return 1.50;
    }

    @Override
    public String getDescription() {
      //to do
        return "Thin Bun Burger";
    }
}
