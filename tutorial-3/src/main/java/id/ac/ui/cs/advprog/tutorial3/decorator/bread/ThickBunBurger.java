package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThickBunBurger extends Food {
    public ThickBunBurger() {
    }

    @Override
    public double cost() {
      //to do
        return 2.50;
    }

    @Override
    public String getDescription() {
      //to do
        return "Thick Bun Burger";
    }
}
