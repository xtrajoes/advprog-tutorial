import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            //show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(each.getCharge()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoint())
                + " frequent renter points";

        return result;
    }

    public String htmlStatement() {

        Iterator<Rental> iterator = rentals.iterator();
        String result = "<H1>Rental Record for <EM>" + getName() + "</EM></H1><P>\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(each.getCharge()) + "<BR>\n";
        }

        // Add footer lines
        result += "<P>You owe <EM>" + String.valueOf(totalAmount()) + "</EM><P>\n";
        result += "On this rental you earned <EM>" + String.valueOf(getTotalFrequentRenterPoint())
                + "</EM> frequent renter points<P>";

        return result;
    }

    private double totalAmount() {
        double total = 0;

        for (Rental each : rentals) {
            total += each.getCharge();
        }
        return total;
    }

    private int getTotalFrequentRenterPoint() {
        int total = 0;

        for (Rental each : rentals) {
            total += each.getFrequentRenterPoints();
        }
        return total;
    }
}
