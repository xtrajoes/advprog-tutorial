import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    private static Movie trialMovie;

    private static Rental rental;

    @Before
    public void setUp() {
        trialMovie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rental = new Rental(trialMovie, 5);
    }

    @Test
    public void getMovie() {

        assertEquals(trialMovie, rental.getMovie());
    }

    @Test
    public void getDaysRented() {

        assertEquals(5, rental.getDaysRented());
    }

}
