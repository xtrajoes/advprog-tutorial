import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


public class MovieTest {

    // Hint: Make the test fixture into an instance variable
    private static Movie firstMovie;
    private static Movie secondMovie;
    private static Movie copyFromFirstMovie;

    @Before
    public void setUp() {
        firstMovie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        secondMovie = new Movie("The Lion King", Movie.CHILDREN);
        copyFromFirstMovie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
    }


    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", firstMovie.getTitle());
    }

    @Test
    public void setTitle() {

        firstMovie.setTitle("Bad Black");

        assertEquals("Bad Black", firstMovie.getTitle());
    }

    @Test
    public void getPriceCode() {

        assertEquals(Movie.REGULAR, firstMovie.getPriceCode());
    }

    @Test
    public void setPriceCode() {

        firstMovie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, firstMovie.getPriceCode());
    }

    @Test
    public void checkEquals() {
        assertTrue(firstMovie.equals(copyFromFirstMovie));
        assertFalse(firstMovie.equals(secondMovie));
    }

    @Test
    public void checkHashCode() {
        assertTrue(firstMovie.hashCode() == copyFromFirstMovie.hashCode());
    }
}
