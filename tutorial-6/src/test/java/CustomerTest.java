import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    private Movie trialMovie1;
    private Movie trialMovie2;
    private Movie trialMovie3;

    private Customer trialCustomer1;
    private Customer trialCustomer2;

    private Rental rental;


    @Before
    public void setUp() {
        trialMovie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        trialMovie2 = new Movie("The Lion King", Movie.CHILDREN);
        trialMovie3 = new Movie("Black Panther", Movie.NEW_RELEASE);
        trialCustomer1 = new Customer("Azhar");
        trialCustomer2 = new Customer("Dhiba");

    }

    @Test
    public void getName() {

        assertEquals("Azhar", trialCustomer1.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        rental = new Rental(trialMovie1, 3);
        trialCustomer1.addRental(rental);

        String result = trialCustomer1.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        rental = new Rental(trialMovie1, 3);
        trialCustomer2.addRental(rental);

        rental = new Rental(trialMovie2, 5);
        trialCustomer2.addRental(rental);

        rental = new Rental(trialMovie3, 2);
        trialCustomer2.addRental(rental);

        String result = trialCustomer2.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 14"));
        assertTrue(result.contains("4 frequent renter points"));

    }

    @Test
    public void htmlStatement() {
        rental = new Rental(trialMovie1, 3);
        trialCustomer1.addRental(rental);
        String htmlResult = trialCustomer1.htmlStatement();

        assertTrue(htmlResult.contains("<P>You owe <EM>3.5"));
        assertTrue(htmlResult.contains("On this rental you earned <EM>1"));

    }

}
