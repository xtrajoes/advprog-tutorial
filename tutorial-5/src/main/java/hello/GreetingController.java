package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                   String name, Model model) {
        if (name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", name + ", I hope you are interested to hire me");
        }

        StringBuilder cv = new StringBuilder();
        cv.append("Name: Jordan Muhammad Andrianda\n");
        cv.append("Birthdate: 06/11/1997\n");
        StringBuilder append = cv.append("Birthplace: Jakarta\n");
        cv.append("Address: Jl.Bambu Apus Raya No 33\n");
        cv.append("Hobby: Ngojay\n");
        cv.append("Education History:\n");
        cv.append("- Elementary School Yasporbi 1 Jakarta 2003-2009\n");
        cv.append("- Junior Highschool Yasporbi 1 Jakarta 2009-2012\n");
        cv.append("- Senior High School 14 Jakarta 2012-2015\n");
        cv.append("- Faculty of Computer Science 2015-Present\n");
        model.addAttribute("cv", cv.toString());

        StringBuilder description = new StringBuilder();
        description.append("I am a ComputerScience Student at Universitas Indonesia. ");
        description.append("I am exprience being a Project Manager on our own Project");
        description.append("My Programming language HTML, CSS, JS, React");
        description.append("Code,Speak,Ngojay");
        model.addAttribute("description", description.toString());

        return "greeting";
    }

}
